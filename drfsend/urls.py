
from .views import ClientCrud,ClientCreate, ResendCreate, ResendCrud, Test
from django.urls import path, include



from sender.yasg import urlpatterns as dic_url

urlpatterns = [
    
    path ('api/v1/get_client/<int:pk>', ClientCrud.as_view(), name="client_profile"),
    path ('api/v1/create_new_client',ClientCreate.as_view(),name ='new_client'),
    path ('api/v1/get_resend/<int:pk>', ResendCrud.as_view(), name="resend_info"),
    path ('api/v1/create_new_resend',ResendCreate.as_view(),name ='new_resend'),
    path ('api/v1/out', Test),
]

urlpatterns += dic_url