from email import message
import imp
from urllib import response
from urllib.request import Request
from django.shortcuts import render
from rest_framework import generics
from.serializers import MessageSerilizer, ClientSerializer, ResendSerilizer, StatisticSerializer
from.models import Client, Message, Reseend, Statistic
import requests
import json
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpRequest, HttpResponse, JsonResponse
import datetime
from rest_framework import status
from .senderAPI import send






class ClientCrud (generics.RetrieveUpdateDestroyAPIView):
    """Получение, обновление и удаление клиента из справочника"""

    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class ClientCreate (generics.CreateAPIView):
    """Создание нового клиента"""
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class ResendCrud (generics.RetrieveUpdateDestroyAPIView):
    """Получение информации о рассылке, её удаление и обновление"""
    queryset = Reseend.objects.all()
    serializer_class = ResendSerilizer

class ResendCreate (generics.CreateAPIView):
    """Создание новой рассылки"""
    queryset = Reseend.objects.all()
    serializer_class = ResendSerilizer

@api_view(["POST"])
def Test(request):
    """Метод отправки сообщений и сбора статистики отправленных сообщений"""
    now = datetime.datetime.now()


    resending = Reseend.objects.filter (start_date__lte = now )
    resending_data = ResendSerilizer(resending, many=True).data
    for resend in resending_data:
       message = Message.objects.filter(resend=resend['id'])
       message_data = MessageSerilizer(message, many=True).data
       for message in message_data:
         client = Client.objects.filter(id=message['client'])
         client_data = ClientSerializer(client, many=True).data
         for client in client_data:
          
          send(client['id'], client['phoneNumber'],resend['text'],  message['id'])
          return Response ('h')
          
    return Response (status = status.HTTP_200_OK)
    

            

      





# Create your views here.
