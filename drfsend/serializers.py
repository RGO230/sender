from dataclasses import field
from pyexpat import model
from rest_framework import serializers
from .models import Reseend, Client, Message, Statistic



class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"

class MessageSerilizer (serializers.ModelSerializer):
    # cllient = serializers.StringRelatedField(many=True)
    # resend = serializers.StringRelatedField(many=True)
    class Meta:
        model = Message
        fields = '__all__'
class ResendSerilizer (serializers.ModelSerializer):
    class Meta:
        model = Reseend
        fields = '__all__'

class StatisticSerializer (serializers.ModelSerializer):
    class Meta:
        model = Statistic
        fields = '__all__'


    
    