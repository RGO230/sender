from ast import operator
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.core.validators import RegexValidator

class Client(models.Model):
    import pytz
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    timezone = models.CharField('TIME_ZONE',max_length=32, choices=TIMEZONES)
    phoneNumberRegex = RegexValidator(regex = r"^7\d{10}")
    phoneNumber = models.CharField('PHONE_NUMBER',validators = [phoneNumberRegex], max_length = 11, unique = True)
    tag = models.CharField ('TAG', max_length=100 )
    operator_code = models.CharField('OPERATOR_CODE',max_length=3, default=0 )

    def save (self,*args,**kwargs):
        self.operator_code = self.phoneNumber [1:4]
        super(Client, self).save(*args, **kwargs)
    

    def __str__(self):
        return self.phoneNumber


class Message (models.Model):
    create_date = models.DateTimeField('CREATE_DATA', auto_now_add=True)
    status = models.BooleanField('STATUS')
    client = models.ForeignKey(Client,related_name='client', on_delete=models.CASCADE)
    resend = models.ForeignKey('Reseend', related_name='resend', on_delete=models.CASCADE)




class Reseend (models.Model):
    start_date = models.DateTimeField('START_DATE',auto_now_add=True)
    text = models.TextField('TEXT')
    end_date = models.DateTimeField('END_DATE')
    

class Statistic (models.Model):
    send_data = models.DateTimeField ('SEND_DATA', auto_now_add=True)
    message = models.ForeignKey(Message, related_name='mes', on_delete=models.CASCADE)

      













 