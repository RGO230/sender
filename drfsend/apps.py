from django.apps import AppConfig


class DrfsendConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'drfsend'
