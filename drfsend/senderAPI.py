from.serializers import  StatisticSerializer
from.models import Statistic
import requests
from rest_framework.response import Response

def send (client_id, phone,message,message_id):
     head = {'accept': 'application/json' ,'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTQ2MTYxNjYsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Impvc2VwaGtuZWNoIn0.UsgNH6ox7W4Wt4e62FZgRe1fcoMO0S3flRwP_WM6hyI' ,'Content-Type': 'application/json'}
     data = { "id": client_id,"phone": phone, "text": message}
     resp = requests.post('https://probe.fbrq.cloud/v1/send/1',headers = head,json = data)
     if resp.status_code == 200:
       serializer = StatisticSerializer(data = message_id)
       if serializer.is_valid():
            serializer.save()
            return Response ('statistic saved')
       else:
             return Response ('statistic error')        
        
        
